# Local Development Image

Just a simple image to hop into with some basic tools and custom user


# Build

```bash
make build
```

# Run

```bash
docker run -it --rm -v ~/code:/home/bmansfield/code bmansfield:devbox-noble-24.04-$sha /bin/bash
```

