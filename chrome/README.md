# Chrome

This is Chrome in a docker container

## Build

Building is pretty straight forward

```bash
docker build -t chrome:${VERSION} .
```

Or use the `Makefile`

```bash
make
```

## Run

Note about running. I usually `alias` this. (see .alias file) Plus it has not been
tested with anything other than Linux. Also last I checked the audio/sounds I could
not get to work on MacOS. You can run it like this on MacOS but you will need to
install XQuartz and have it running in order for it to work.

Please note the mounted volumes. I created separate `~/ctnr-chrome-data/` and
`~/Downloads-ctnr-chrome` directories for the container so that I can not have
conflicts with my installed version. This will also allow me to run different
configurations.

See `/.alias` file. `source .alias` or

```bash
xhost +; docker run -it --rm --net host --cpuset-cpus 0 --memory 2g -e DISPLAY=unix$DISPLAY --security-opt seccomp=$HOME/chrome.json --device /dev/snd --device /dev/dri -v /tmp/.X11-unix:/tmp/.X11-unix -v /dev/shm:/dev/shm -v $HOME/Downloads-ctnr-chrome:/home/chrome/Downloads -v /var/run/dbus/:/run/dbus/:rw --name chrome bmansfield/chrome:latest

```


## Dockerhub

I also push the image so I can just simply pull it on another device if I don't
feel like building it.

```bash
docker push chrome:${VERSION}
```

Or

```bash
make publish
```

Alternatively if you want to pull the image

```bash
docker pull chrome:${VERSION}
```

Or

```bash
make pull
```
