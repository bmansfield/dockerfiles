# Dockerfiles

This project is for common docker projects that I use for various purposes.

Mostly just the Firefox one right now. The others are using official images, they
more just my docker-compose setups so I can run them locally.
