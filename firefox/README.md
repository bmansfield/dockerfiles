# Firefox

This is Firefox in a docker container

## Build

Building is pretty straight forward

```bash
docker build -t firefox:${VERSION} .
```

Or use the `Makefile`

```bash
make
```

## Run

Note about running. I usually `alias` this. (see .alias file) Plus it has not been
tested with anything other than Linux. Also last I checked the audio/sounds I could
not get to work on MacOS. You can run it like this on MacOS but you will need to
install XQuartz and have it running in order for it to work.

Please note the mounted volumes. I created separate `~/.mozilla` and
`~/Download` directories for the container so that I can not have conflicts with
my installed version. This will also allow me to run different configurations.

See `/.alias` file. `source .alias` or

```bash
xhost +; docker run -it -v /tmp/.X11-unix:/tmp/.X11-unix -v $HOME/.mozilla-ctnr:/root/.mozilla -v $HOME/Downloads-ctnr:/root/Downloads -e DISPLAY=unix$DISPLAY --device /dev/snd --shm-size 2g --dns 9.9.9.9 --dns 149.112.112.112 --privileged firefox:${VERSION}
```


## Dockerhub

I also push the image so I can just simply pull it on another device if I don't
feel like building it.

```bash
docker push firefox:${VERSION}
```

Or

```bash
make publish
```

Alternatively if you want to pull the image

```bash
docker pull firefox:${VERSION}
```

Or

```bash
make pull
```


## Updating

The way I have been updating it is running a blank container and installing firefox
and seeing the version. Then I will update the `Dockerfile` with that specific
version and rebuild and push the image.


## Other Notes

What I have been doing for updates is; building it with a specific version tag,
and next update I'll just rebuild with a new version tag. That way I can keep
and run previous versions if I needed or wanted to. I have found that sometimes
newer versions do not work right out of the box and require some tweaking or
research to get to run smoothly.
