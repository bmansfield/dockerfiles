FROM debian:sid-slim

ARG VERSION=88
ARG PACKAGE_VERSION=88.0.1-1

LABEL maintainer="Byron Mansfield - protobyte@tuta.io" \
      name="Firefox ${VERSION} Container" \
      description="Firefox ${VERSION} GUI Docker Container" \
      version="${VERSION}"

ENV LANG="en-US" \
    LANGUAGE="en_US:en" \
    LC_ALL="en_US.UTF-8"

COPY local.conf /etc/fonts/local.conf
COPY entrypoint.sh /usr/bin/startfirefox

RUN apt-get update && \
    apt-get install -y \
            --no-install-recommends \
            apulse \
            ca-certificates \
            dirmngr \
            gnupg \
            ffmpeg \
            firefox=${PACKAGE_VERSION} \
            hicolor-icon-theme \
            libasound2 \
            libavcodec58 \
            libcanberra0 \
            libegl-dev \
            libgl1-mesa-dri \
            libgl1-mesa-glx \
            libgssapi-krb5-2 \
            libgtk-3-0 \
            libpci3 \
            libpulse0 \
            fonts-lmodern \
            fonts-stix \
            fonts-noto \
            fonts-noto-cjk \
            fonts-noto-color-emoji \
            mozplugger \
            pulseaudio && \
    \
    # clear apt cache
    rm -rf /var/lib/apt/lists/* && \
    \
    # set some basic firefox preferences
    echo 'pref("browser.tabs.remote.autostart", false);' >> /etc/firefox/syspref.js

ENTRYPOINT [ "startfirefox" ]
