# Gcloud Min

This is a utility image based on alpine that has many other common tooling in it
such has kubectl and helm. However, if you install gcloud the typical way on alpine
it is pretty large. I found a way to minimize it to make the image even smaller
which is part of the reason people use alpine is for how small of a footprint it
has.

