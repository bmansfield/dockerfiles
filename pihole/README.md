# Pihole

Pihole in a docker container to run locally.


# Configure

There are a few configurations you might want to change in the `docker-compose.yml`

Under `environment` you will likely want to adjust the `TZ` value to your local
timezone. Also `WEBPASSWORD` to a more secure password. This will be the password
you use to login to the web admin interface.

Other configurations you may want to consider would be the pihole's DNS. As is it
is set to Cloudflare's name resolvers `1.1.1.1` and `1.0.0.1`. But you can change
this to any DNS you want. You can also change this in the web admin UI.


# Run

To run, create these two directories (`etc-dnsmasq.d`, `etc-pihole`) in the same
directory as `docker-compose.yml` file. Then run the following command;

```bash
docker-compose up -d
```

Then you will need to point your DNS to the pihole container once it is up and
running.

```bash
cat /etc/resolv.conf

nameserver 127.0.0.1
```

Now all of your traffic should be running through your containerized pihole. If
at anytime you stop it, you will need to swap your DNS (i.e. `/etc/resolv.conf`)
back or you will be disconnected.

To stop simply run `docker-compose down` in the same directory of your `docker-compose.yml`
file.


# Web Admin UI

If you want to view pihole web admin UI you can navigate to `localhost/admin` in
your web browser. To login use the password you set in the `docker-compose.yml`
file under `environment` > `WEBPASSWORD`.

Once logged in you can set your blocklist under `Settings` > `Blocklists` tab.
