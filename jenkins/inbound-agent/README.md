# Jenkins Inbound-Agent

This is a Jenkins Inbound-Agent Docker Container to be spun up for jobs. It is based off
of the official jenkins inbound-agent [Docker Hub here](https://hub.docker.com/r/jenkins/inbound-agent/)
and it's [Github here](https://github.com/jenkinsci/docker-inbound-agent). It may periodically
need to be updated.

You can update this with a `VERSION` make argument, but please also follow up
with updating the default version in the `Makefile` and the `Dockerfile` as
well.


There is a `Makefile` for convenience.

```bash
cd <this-dir>
make build
```

If you want to build a new version on the fly, you can

```bash
cd <this-dir>
make build VERSION=<MY-NEW-VERSION>
```

But please keep in mind. There are defaults set in the `Makefile` and
`Dockerfile`. If your new updated version is working and you wish to use it
moving forward, please update those defaults so that the next developer can also
build the correct version.


## Push

If everything looks ok with your new docker image and you are ready to push it
to ECR

```bash
cd <this-dir>
make push
```

If you get an error about not authorized to push to ecr

```bash
make login
```

Then try pushing again. Please check to see if your work is in ECR.


## Deploy

Unlike the master Jenkins you do not have to redeploy via helm.

