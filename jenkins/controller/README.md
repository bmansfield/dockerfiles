# Jenkins Master

This is the Jenkins Master Docker Image. It is based off of the official jenkins
docker image. You can find it's [Docker Hub here](https://hub.docker.com/r/jenkins/jenkins)
and it's [Github here](https://github.com/jenkinsci/docker). It may periodically
need to be updated. Please check for depreciation warning on their respected `README.md`'s.

If you need to update the image to newer version please inspect the docker tags
in the dockerhub. It was chosen not to do the `lts` tag but rather the version
for clarity, verbosity, and sometimes the `lts` version is newer and we would
rather make the decision to manually control this in case of breaking changes
which may not be desired.

If you are updating this `Dockerfile` please also update any of those changes to
the `inbound-agent` as well. As they are tightly coupled and most of the jobs are
going to be executed in the `inbound-agent` pod.


## Build

There is a `Makefile` for convenience.

```bash
cd <this-dir>
make build
```

If you want to build a new version on the fly, you can

```bash
cd <this-dir>
make build JENKINS_VERSION=<MY-NEW-VERSION>
```

But please keep in mind. There are defaults set in the `Makefile` and
`Dockerfile`. If your new updated version is working and you wish to use it
moving forward, please update those defaults so that the next developer can also
build the correct version.


## Push

If everything looks ok with your new docker image and you are ready to push it
to ECR, or Harbor, or gitlab, whatever we end up deciding to go with. `Makefile`
will need to be updated accordingly.

```bash
cd <this-dir>
make push
```

If you get an error about not authorized to push to ecr

```bash
make login
```

Then try pushing again. Please check to see if your work is in ECR.


## Deploy

If you want to deploy jenkins using this new version you just built, you will
need to go to the helm directory.

```bash
cd </path/to/helm/jenkins>
make install
```

Please be courteous to your coworkers and communicate the changes you just did
and your intentions of redeploying the jenkins server to ensure you are not
disrupting their work.


## Notes

A few special gotchya's that you might want to be aware of.

The `docker build ...` command is using docker's new build kit feature. This is
what helps us use features such as `--chmod` in the `COPY` step in order to reduce
layers.

Also, it does build with pre-defined plugins. This is only if you want to, or for
a matter of convenience, so that you can run it outside of of the jenkins Helm
chart (which has the install plugins init-container) with some basic plugins.
The official Jenkins Helm Chart you can turn on and off installing plugins overtop
of the already installed one's in the docker image.

Another word about jenkin's plugins. I know it's tempting to use the `latest` version,
but you will find out quickly, that is not a good choice. You can end up in very
bad situations, and for whatever reason, many plugins do not repoint their `latest`
to the actual latest. It's much better to be explicit. You can always use the `GUI`
plugin management system to update at any time. The ones committed right now likely
need to be updated anyways.

