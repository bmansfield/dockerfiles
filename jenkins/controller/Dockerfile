ARG JENKINS_VERSION=2.429
FROM jenkins/jenkins:${JENKINS_VERSION} as base

ARG BUILD_DATE
ARG BUILD_NUMBER=1

LABEL maintainer="devops@acme.com" \
      description="This is a base jenkins master image, which allows connecting Jenkins agents via JNLP protocols" \
      vendor="ACME Co" \
      version="${JENKINS_VERSION}" \
      build-date="${BUILD_DATE}" \
      build-number="${BUILD_NUMBER}"

ENV JAVA_OPTS="-Djenkins.install.runSetupWizard=false" \
    COPY_REFERENCE_FILE_LOG="${JENKINS_HOME}/copy_reference_file.log" \
    BUILD_DATE="${BUILD_DATE}" \
    BUILD_NUMBER="${BUILD_NUMBER}" \
    JENKINS_VERSION="${JENKINS_VERSION}"

COPY --chmod=755 scriptApproval.xml /usr/share/jenkins/ref/scriptApproval.xml
COPY list_tags.sh /var/lib/jenkins/bin/

WORKDIR ${JENKINS_HOME}

VOLUME ${JENKINS_HOME}

USER root

RUN apt update && \
    apt install -y \
                --no-install-recommends \
                apt-transport-https \
                build-essential \
                ca-certificates \
                curl \
                dnsutils \
                less \
                libssl-dev \
                libffi-dev \
                libnghttp2-dev \
                git \
                gpg \
                gnupg2 \
                netcat-traditional \
                net-tools \
                nghttp2 \
                python3 \
                python3-boto3 \
                python3-dev \
                python3-pip \
                python3-poetry \
                python3-venv \
                python3-virtualenv \
                redis-server \
                rsync \
                software-properties-common \
                telnet \
                traceroute \
                unzip \
                wget \
                whois \
                vim \
                zip && \
    \
    # clean up
    rm -rf /var/lib/apt/lists/* && \
    apt clean

FROM base as docker

ARG KUBECTL_VERSION=v1.28.2
ARG HELM_VERSION=v3.13.1
ARG EKSCTL_VERSION=v0.162.0

ENV KUBECTL_VERSION=${KUBECTL_VERSION} \
    HELM_VERSION=${HELM_VERSION} \
    EKSCTL_VERSION=${EKSCTL_VERSION}

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" && \
    apt update && \
    apt install -y \
                --no-install-recommends \
                docker-ce \
                docker-ce-cli \
                containerd.io && \
    \
    # clean up
    rm -rf /var/lib/apt/lists/* && \
    apt clean && \
    \
    # add jennkins to docker group
    usermod -a -G docker jenkins && \
    \
    # kubectl
    curl -LO https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl && \
    chmod +x ./kubectl && \
    mv ./kubectl /usr/local/bin/kubectl && \
    chown root:docker /usr/local/bin/kubectl && \
    \
    # Install Helm
    curl -LO https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    tar -xvf helm-${HELM_VERSION}-linux-amd64.tar.gz && \
    mv linux-amd64/helm /usr/local/bin/helm && \
    chmod +x /usr/local/bin/helm && \
    rm -fr linux-amd64 && \
    \
    # eksctl
    curl -LO https://github.com/eksctl-io/eksctl/releases/download/${EKSCTL_VERSION}/eksctl_$(uname -s)_amd64.tar.gz && \
    tar -xvf eksctl_$(uname -s)_amd64.tar.gz && \
    chmod +x ./eksctl && \
    mv ./eksctl /usr/local/bin/eksctl

FROM docker as tools

ARG TERRAFORM_VERSION=1.6.2

ENV TERRAFORM_VERSION=${TERRAFORM_VERSION}

COPY --chown=jenkins:jenkins .ssh /var/jenkins_home/.ssh

# AWS cli
RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip" && \
    unzip awscliv2.zip && \
    ./aws/install && \
    \
    # install ansible
    apt update && \
    apt install -y ansible && \
    \
    # hashicorp tools
    #
    # old way seems broken
    # curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    # apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    #
    # new way
    wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor -o /usr/share/keyrings/hashicorp-archive-keyring.gpg && \
    echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | tee /etc/apt/sources.list.d/hashicorp.list && \
    \
    apt update && \
    apt install -y \
                --no-install-recommends \
                jq \
                packer \
                terraform \
                vault && \
    # clean up
    \
    rm -rf /var/lib/apt/lists/* && \
    apt clean && \
    \
    # ssh permissions
    chmod -R 700 /var/jenkins_home/.ssh && \
    chmod 644 /var/jenkins_home/.ssh/* && \
    chmod 600 /var/jenkins_home/.ssh/id_ed25519

USER jenkins

COPY --chown=jenkins:jenkins plugins.txt /usr/share/jenkins/ref/plugins.txt

RUN jenkins-plugin-cli -f /usr/share/jenkins/ref/plugins.txt
