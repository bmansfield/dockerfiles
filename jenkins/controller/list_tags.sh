#!/bin/bash

#
# List tags for a docker image in ECR
#
# usage: /var/lib/jenkins/bin/list_tags.sh <ENV> <REPO>
#


set -e


ENV=$1
REPO=$2
REGION="us-west-2"
ACCT="123456789012"


tags=$(aws ecr list-images \
            --repository-name "$REPO" \
            --region "$REGION" | \
            jq -r '.imageIds[].imageTag' | \
                grep -v latest | sort -V)

# output tags
for tag in "$tags"; do
    echo "$tag"
done

