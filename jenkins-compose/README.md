# Jenkins

Jenkins in a container. You can run your own jenkins instance locally inside a
docker container with a persistent configuration.


# Run

Before you run it for the first time, create a jenkins home directory in the same
path that this `docker-compose.yml` file is.

```bash
cd /some/directory/
mkdir -p jenkins_home
```

Then you can run it with docker-compose.

```bash
docker-compose up -d
```

If you need to stop it, just `cd` back into the directory where this `docker-compose.yml`
lives and run

```bash
docker-compose down
```


# Access

To access your this jenkins in a container, open your web browser and navigate to
`localhost:8081`.

Your first time logging in to this jenkins you will need to give it an admin password
which can be found here. You will then need to create your username and password.
You will use this to log into jenkins each time you launch this container.


# Notes

This is setup to be persistent and it will store all of the user login info, job
configs, addons, and everything else jenkins needs to run under the `jenkins_home`
directory you setup earlier. If you delete this folder then it will be like a new
fresh jenkins. Or if you need to migrate it to somewhere else, just copy over the
`jenkins_home` directory along with all of it's contents.
