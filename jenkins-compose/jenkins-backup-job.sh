#!/bin/bash

DATE=`date '+%Y-%m-%d-%H:%M:%S'`


# Create a directory for the job definitions
mkdir -p $BUILD_ID/jobs

# Copy global configuration files into the workspace
cp $JENKINS_HOME/*.xml $BUILD_ID/

# Copy keys and secrets into the workspace
cp $JENKINS_HOME/secret.key $BUILD_ID/
cp $JENKINS_HOME/secret.key.not-so-secret $BUILD_ID/
cp -r $JENKINS_HOME/secrets $BUILD_ID/

# Copy user configuration files into the workspace
cp -r $JENKINS_HOME/users $BUILD_ID/

cp -r $JENKINS_HOME/plugins $BUILD_ID/

# Copy job definitions into the workspace
rsync -am --include='config.xml' --include='*/' --prune-empty-dirs --exclude='*' $JENKINS_HOME/jobs/ $BUILD_ID/jobs/

# Create an archive from all copied files (since the CS plugin cannot copy folders recursively)
tar czf ./jenkins-configuration-${DATE}.tar.gz $BUILD_ID/

# Remove the directory so only the archive gets copied to CS
rm -rf $BUILD_ID

gsutil cp ${WORKSPACE}/jenkins-configuration-${DATE}.tar.gz gs://${DEVOPS_CS_BUCKET}/jenkins_backups/
